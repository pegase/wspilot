## directory  for hackathon related documents : rdfobj

### install on linux (need network)

from this directory, using a terminal:

```
docker-compose down
docker-compose build
docker-compose up
```
with your browser, go to 
```
http://127.0.0.1:8888/
```
password is 'pass'

then onpen a notebook:
```
http://127.0.0.1:8888/lab/tree/script/custom_onto_with_biopax.ipynb

```