#!/bin/bash

# Function to check if sudo is installed
IFILE="/fuseki/custom_init.lock"
#SHIROCONF="/jena-fuseki/shiro.ini"

check_sudo() {
    if command -v sudo &> /dev/null; then
        echo "sudo is already installed."
    else
        echo "sudo is not installed. Installing sudo..."
        #apk update
        #apk add sudo
        apt-get update
        apt-get install -y sudo 

        if command -v sudo &> /dev/null; then
            echo "sudo has been installed successfully."
        else
            echo "Failed to install sudo. Exiting."
            exit 1
        fi
    fi
}


# Check if script is run as root
if [ "$EUID" -ne 0 ]; then
    echo "Please run as root."
    exit 1
fi

# Check and install sudo if necessary
check_sudo

echo  "--------custom fuseki start 1--------"  


# Define the processInit function
processInit() {
    echo "Initializing process..."
    # Your initialization code goes here


    ls -Rl  /fuseki
    chmod -R 777 /fuseki
    cp -r /db_tmp/* /fuseki/
    #mv /fuseki/shiro.ini  $SHIROCONF

}

# Define the check_and_init function
check_and_init() {
    if [ -f ${IFILE} ]; then
        echo "${IFILE} exists. Initialization not required."
    else
        echo "${IFILE} does not exist. Calling processInit..."
        processInit
        touch ${IFILE}
        echo "Created ${IFILE} to indicate initialization is done."
    fi
}

check_and_init

ls -l /jena-fuseki 
#cat $SHIROCONF
echo "ADMIN_PASSWORD:${ADMIN_PASSWORD}"
echo  "--------custom fuseki start 2--------"  

rm -f /fuseki/system/*.lock

##
##sudo -u fuseki  /jena-fuseki/fuseki-server
 
/script/startup.sh

