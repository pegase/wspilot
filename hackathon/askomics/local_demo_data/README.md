# Available datasets

[DE file](https://zenodo.org/record/2529117/files/limma-voom_luminalpregnant-luminallactate)
[GFF file](https://zenodo.org/record/3601076/files/Mus_musculus.GRCm38.98.subset.gff3)
[Gene/Ensembl correspondance file](https://zenodo.org/record/3601076/files/symbol-ensembl.tsv)
[QTL file](https://zenodo.org/record/3601076/files/MGIBatchReport_Qtl_Subset.txt)
