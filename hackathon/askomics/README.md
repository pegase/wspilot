Available Askomics instance:

https://test-192-168-100-87.vm.openstack.genouest.org/


# Folder contents

## docker_files

This folder contains a docker-compose.yml file, and all the configuration files required to run a small-scale askomics instance

## local_demo_data

List of files to use with the [local tutorial](https://flaskomics.readthedocs.io/en/latest/tutorial/)

# Available tutorials

## Local tutorial

https://flaskomics.readthedocs.io/en/latest/tutorial/

## Use-galaxy tutorial (federation)

https://training.galaxyproject.org/training-material/topics/transcriptomics/tutorials/rna-seq-analysis-with-askomics-it/tutorial.html

## CLI tutorial

Python library + CLI: [askoclics][https://github.com/askomics/askoclics]
Installation:

```
pip install askoclics
```

Usage:

Init instance:

```
# On first use you'll need to create a config file to connect to the server, just run:

$ askoclics init
Welcome to Askoclics
Askomics server url, including http:// and the port if required: http://0.0.0.0:80
Askomics user's API key: XXXXXXX
Testing connection...
Ok! Everything looks good.
Ready to go! Type `askoclics` to get a list of commands you can execute.
```

List all files:

```
$ askoclics file list
[
    {
        "name": "qtl.tsv",
        "size": 99,
        "type": "csv/tsv",
        "id": 2,
        "date": 1612957604
    },
    {
        "name": "gene.gff3",
        "size": 2267,
        "type": "gff/gff3",
        "id": 3,
        "date": 1612957604
    },
    {
        "name": "gene.bed",
        "size": 689,
        "type": "bed",
        "id": 4,
        "date": 1612957604
    }
]

$ askoclics file describe 4
[
    {
        "data": {
            "columns_type": [
                "start_entity",
                "text",
                "text",
                "date"
            ],
            "header": [
                "Person",
                "foaf:firstName",
                "gender",
                "foaf:birthdate"
            ]
        },
        "error": false,
        "error_message": "",
        "id": 4,
        "name": "Person-sex-yearOfBirth.tsv",
        "type": "csv/tsv"
    }
]
```
Listing datasets:

```
$ askoclics dataset list
[
    {
        "end": 1719822867,
        "error_message": "",
        "exec_time": 0,
        "id": 7,
        "name": "Person-sex-yearOfBirth.tsv",
        "ntriples": 69,
        "ontology": 0,
        "percent": 100.0,
        "public": false,
        "start": 1719822867,
        "status": "success",
        "traceback": null
    }
]
```

Integrating a csv file

```
$ askoclics file integrate_csv -h
Usage: askoclics file integrate_csv [OPTIONS] FILE_ID

  Send an integration task for a specified file_id

  Output:

      Dictionary of task information

Options:
  --columns TEXT     Comma-separated columns (default to detected columns)
  --headers TEXT     Comma-separated headers (default to file headers)
  --force            Ignore the content type mismatch (ex: force an integer
                     type when AskOmics detects a text type)
  --custom_uri TEXT  Custom uri
  --skip_preview     Skip the preview step for big files
  -h, --help         Show this message and exit.

  askoclics file integrate_csv 4
  {
      "dataset_ids": [
          10
      ],
      "error": false,
      "errorMessage": ""
  }
```
