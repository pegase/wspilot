# AskOmics - docker-compose file

Some docker-compose files to deploy AskOmics and all its dependencies.

## Install docker and docker-compose

```bash
# docker
sudo curl -sSL https://get.docker.com/ | sh
# docker-compose
# Debian/ubuntu
sudo apt install -y docker-compose
```

## [Federated](federated)

Deploy AskOmics with a federated query engine to perform queries on multiple endpoints

- flaskomics
- celery-flaskomics (flaskomics worker)
- redis (database, celery dependency)
- virtuoso (triplestore)
- corese (federated query engine)
- nginx (web proxy)


## Run AskOmics

Run docker

```bash
docker-compose up -d
```

Askomics will be deployed on port 80 (localhost)

See logs

```bash
docker-compose logs -f
```
