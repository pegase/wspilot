Steps to integrate a partial GO ontology (as an admin on an AskOmics instance)

* Integrate go_light.owl
* Integrate go_abstraction.owl

In the admin tab, create an ontology with the following parameters

Ontology name: GO
Short name: GO
URI: http://purl.obolibrary.org/obo/go#
Linked public dataset: go_light.owl
Label uri: rdfs:label
Autocomplete type: local

And click 'create'
You can then integrate 'ex_entity.csv', specifying 'GO' as the column type for 'goterm'
